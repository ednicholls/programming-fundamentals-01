# A node.js application using express.js and SQLite to upload and download files

- This has all been committed to git @ https://bitbucket.org/ednicholls/programming-fundamentals-01/src/master/

- On the main screen you can access the upload and download pages, as well as seeing the file details

- When you upload a file, it tells you the name and format of the file you just uploaded

- If you upload the same file the file is not duplicated and is replaced

- In file details you can see what exact time and date you uploaded the file

