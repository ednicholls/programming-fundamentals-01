// start SQLite
var sqlite3 = require('sqlite3').verbose();
// file name of database
const DBSOURCE = "db.sqlite";
// opening database
let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message);
        throw err
    } else {
        console.log('Connected to the SQLite database.');
        // attempt to create table 'file_details'
        db.run(`CREATE TABLE file_details
                (
                    id             INTEGER PRIMARY KEY AUTOINCREMENT,
                    name           text UNIQUE,
                    uploadDatetime text,
                    size           INTEGER,
                    mimetype       text,
                    CONSTRAINT name_unique UNIQUE (name)
                )`,
            (err) => {
                if (err) {
                    // Table already created
                } else {
                    // Table just created, insert some rows?
                }
            });
    }
});

// exports database
module.exports = db;