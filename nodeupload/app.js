// load express.js framework and start listening on port 8080
var express = require("express"),
    app = express(),
    http = require("http").Server(app).listen(8080),
    upload = require("express-fileupload");
app.use(upload());

console.log("The server is up and running");
// define handler for GET /
app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html"); // display menu
});
// define handler for GET /upload
app.get("/upload", function (req, res) {
    res.sendFile(__dirname + "/upload.html"); // display upload form
});
// define handler for POST /upload
app.post("/upload", function (req, res) {
    // if the request has files
    if (req.files) {
        console.log(req.files);
        // get attributes off file
        var file = req.files.file0,
            filename = file.name,
            mimetype = file.mimetype,
            size = file.size;
        // moving file to upload directory and define callback
        file.mv("./upload/" + filename, function (err) {
            // if error, display message
            if (err) {
                console.log(err);
                res.send("error occurred");
            } else {
                // no error display success
                res.send("You have successfully uploaded your file '" + filename + "'!" + "<br />" +
                    size + " " + mimetype + '<br /><a href="/">menu</a>');
                // update database with details of file
                var replace = 'REPLACE INTO file_details (name, uploadDatetime, size, mimetype) VALUES (?,?,?,?)',
                    d = new Date();
                db.run(replace, [filename, d.toISOString(), size, mimetype]);
            }
        })
    }
});
// define handler for GET /file_details
app.get("/file_details", (req, res, next) => {
    var sql = "select * from file_details";
    var params = [];
    // display all rows from table 'file_details' as json
    db.all(sql, params, (err, rows) => {
        // if error show error message and 400 bad request
        if (err) {
            res.status(400).json({"error": err.message});
            return;
        }
        // display results in json
        res.json({
            "message": "success",
            "data": rows
        })
    });
});

// load 'serve-index' which will handle file downloads based on files in the upload directory
var serveIndex = require('serve-index');

app.use('/download', serveIndex('./upload/', {icons: true})); // shows you the file list
app.use('/download', express.static('./upload/')); // serve the actual files

// define connection to SQLite
var db = require('./database.js');
